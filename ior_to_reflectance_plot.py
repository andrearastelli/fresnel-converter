import matplotlib

# matplotlib.use('Qt4Agg')
# matplotlib.rcParams['backend.qt4'] = 'PySide'

matplotlib.use('Agg')

import numpy
import matplotlib.pyplot
import matplotlib.ticker
import ior_to_reflectance


t1 = numpy.arange(0.0, 91.0, 1)

n_red = 0.12550
k_red = 3.7923

n_green = 0.32393
k_green = 2.5972

n_blue = 1.5486
k_blue = 1.9107

refRpr, refRpg, refRpb = [], [], []
refRsr, refRsg, refRsb = [], [], []
refRr, refRg, refRb = [], [], []

for t in t1:
    t = numpy.deg2rad(t)
    Rp_red = ior_to_reflectance.Rp(t, n_red, k_red)
    Rp_green = ior_to_reflectance.Rp(t, n_green, k_green)
    Rp_blue = ior_to_reflectance.Rp(t, n_blue, k_blue)

    Rs_red = ior_to_reflectance.Rs(t, n_red, k_red)
    Rs_green = ior_to_reflectance.Rs(t, n_green, k_green)
    Rs_blue = ior_to_reflectance.Rs(t, n_blue, k_blue)

    refRpr.append(Rp_red)
    refRpg.append(Rp_green)
    refRpb.append(Rp_blue)

    refRsr.append(Rs_red)
    refRsg.append(Rs_green)
    refRsb.append(Rs_blue)

    refRr.append(ior_to_reflectance.R(Rp_red, Rs_red))
    refRg.append(ior_to_reflectance.R(Rp_green, Rs_green))
    refRb.append(ior_to_reflectance.R(Rp_blue, Rs_blue))

# print dict(enumerate(refRsb)).get(60)
# matplotlib.pyplot.xkcd(scale=0.5)
# matplotlib.pyplot.winter()
# matplotlib.pyplot.viridis()

matplotlib.pyplot.subplot(311)
matplotlib.pyplot.plot(t1, refRpr, label='Rp red')
matplotlib.pyplot.plot(t1, refRsr, label='Rs red')
matplotlib.pyplot.plot(t1, refRr, label='R red')
matplotlib.pyplot.grid(True)
matplotlib.pyplot.xlim(0, 90)
matplotlib.pyplot.ylim(0, 1)
matplotlib.pyplot.legend(ncol=3)

matplotlib.pyplot.subplot(312)
matplotlib.pyplot.plot(t1, refRpg, label='Rp green')
matplotlib.pyplot.plot(t1, refRsg, label='Rs green')
matplotlib.pyplot.plot(t1, refRg, label='R green')
matplotlib.pyplot.grid(True)
matplotlib.pyplot.xlim(0, 90)
matplotlib.pyplot.ylim(0, 1)
matplotlib.pyplot.legend(ncol=3)

matplotlib.pyplot.subplot(313)
matplotlib.pyplot.plot(t1, refRpb, label='Rp blue')
matplotlib.pyplot.plot(t1, refRsb, label='Rs blue')
matplotlib.pyplot.plot(t1, refRb, label='R blue')
matplotlib.pyplot.grid(True)
matplotlib.pyplot.xlim(0, 90)
matplotlib.pyplot.ylim(0, 1)
matplotlib.pyplot.legend(ncol=3)

# matplotlib.pyplot.plot(60, dict(enumerate(refRb)).get(60), 'r+')

# matplotlib.pyplot.axis([0, 90, 0, 1])


# matplotlib.pyplot.show()
matplotlib.pyplot.savefig('rgbGold.png')
matplotlib.pyplot.close()
# matplotlib.pyplot.show()
