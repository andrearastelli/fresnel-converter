import sys

from PySide import QtGui


################################################################################
# @brief      Class for reflectance plot curve ui.
#
class ReflectancePlotCurveUI(QtGui.QWidget):

    ############################################################################
    # @brief      Constructs the object.
    #
    # @param      self  The object
    #
    def __init__(self):
        super(ReflectancePlotCurveUI, self).__init__()

    ############################################################################
    # @brief      { function_description }
    #
    # @param      self  The object
    #
    # @return     { description_of_the_return_value }
    #
    def paintEvent(self, event):
        pass

    ############################################################################
    # @brief      Draws a line.
    #
    # @param      self  The object
    # @param      qp    { parameter_description }
    #
    # @return     { description_of_the_return_value }
    #
    def drawLine(self, painter):
        pass


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    plotCurve = ReflectancePlotCurveUI()
    plotCurve.show()
    sys.exit(app.exec_())
