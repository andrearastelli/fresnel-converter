import math

material = 'Aluminium'

n_red = 1.3456
k_red = 7.4746

n_green = 0.96521
k_green = 6.3995

n_blue = 0.61722
k_blue = 5.3031


################################################################################
# @brief      { function_description }
#
# @param      theta  The theta
# @param      n      { parameter_description }
# @param      k      { parameter_description }
#
# @return     { description_of_the_return_value }
#
def Rp(theta, n, k):
    cosTheta = math.cos(theta)
    num = pow((n - 1 / cosTheta), 2) + pow(k, 2)
    den = pow((n + 1 / cosTheta), 2) + pow(k, 2)
    return num / den


################################################################################
# @brief      { function_description }
#
# @param      theta  The theta
# @param      n      { parameter_description }
# @param      k      { parameter_description }
#
# @return     { description_of_the_return_value }
#
def Rs(theta, n, k):
    cosTheta = math.cos(theta)
    num = pow((n - cosTheta), 2) + pow(k, 2)
    den = pow((n + cosTheta), 2) + pow(k, 2)
    return num / den


################################################################################
# @brief      { function_description }
#
# @param      Rp    { parameter_description }
# @param      Rs    { parameter_description }
#
# @return     { description_of_the_return_value }
#
def R(Rp, Rs):
    return (Rp + Rs) * 0.5


# for degree in xrange(0, 100, 10):

#     theta = math.radians(degree)
#     ref_p_red = Rp(theta, n_red, k_red)
#     ref_s_red = Rs(theta, n_red, k_red)

#     ref_p_green = Rp(theta, n_green, k_green)
#     ref_s_green = Rs(theta, n_green, k_green)

#     ref_p_blue = Rp(theta, n_blue, k_blue)
#     ref_s_blue = Rs(theta, n_blue, k_blue)

#     print '{:02}\t{:<10.4}{:<10.4}{:<10.4}'.format(degree, R(ref_p_red, ref_s_red), R(ref_p_green, ref_s_green), R(ref_p_blue, ref_s_blue))
